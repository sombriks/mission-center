/* ui/performance_page/memory_details.blp
 *
 * Copyright 2023 Romeo Calota
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Gtk 4.0;

Box root {
  margin-top: 65;
  margin-bottom: 10;
  orientation: vertical;
  spacing: 20;

  Box dynamic_data {
    orientation: vertical;
    spacing: 10;

    Box top_row {
      spacing: 15;

      Box {
        orientation: vertical;
        spacing: 3;
        width-request: 100;

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("In use");
        }

        Label in_use {
          styles [
            "title-4",
          ]

          halign: start;
        }
      }

      Box {
        orientation: vertical;
        spacing: 3;
        width-request: 100;

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Available");
        }

        Label available {
          styles [
            "title-4",
          ]

          halign: start;
        }
      }
    }

    Box mid_row {
      spacing: 15;

      Box {
        orientation: vertical;
        spacing: 3;
        width-request: 100;

        Label {

          styles [
            "caption",
          ]

          halign: start;
          label: _("Committed");
        }

        Label committed {
          styles [
            "title-4",
          ]

          halign: start;
        }
      }

      Box {
        orientation: vertical;
        spacing: 3;
        width-request: 100;

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Cached");
        }

        Label cached {
          styles [
            "title-4",
          ]

          halign: start;
        }
      }
    }

    Box bottom_row {
      spacing: 15;

      Box {
        orientation: vertical;
        spacing: 3;
        width-request: 100;

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Swap available");
        }

        Label swap_available {
          styles [
            "title-4",
          ]

          halign: start;
        }
      }

      Box {
        orientation: vertical;
        spacing: 3;
        width-request: 100;

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Swap used");
        }

        Label swap_used {
          styles [
            "title-4",
          ]

          halign: start;
        }
      }
    }
  }

  Box system_info {
    spacing: 10;

    Box labels {
      orientation: vertical;
      spacing: 3;

      Label {
        styles [
          "caption",
        ]

        halign: start;
        label: _("Speed:");
      }

      Label {
        styles [
          "caption",
        ]

        halign: start;
        label: _("Slots used:");
      }

      Label {
        styles [
          "caption",
        ]

        halign: start;
        label: _("Form factor:");
      }

      Label {
        styles [
          "caption",
        ]

        halign: start;
        label: _("Type:");
      }
    }

    Box values {
      orientation: vertical;
      spacing: 3;

      Label speed {
        styles [
          "caption",
        ]

        halign: start;
        use-markup: true;
      }

      Label slots_used {
        styles [
          "caption",
        ]

        halign: start;
        use-markup: true;
      }

      Label form_factor {
        styles [
          "caption",
        ]

        halign: start;
        use-markup: true;
      }

      Label ram_type {
        styles [
          "caption",
        ]

        halign: start;
        use-markup: true;
      }
    }
  }
}
